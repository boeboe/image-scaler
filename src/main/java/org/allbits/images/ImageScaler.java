package org.allbits.images;

import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.ResampleOp;
import org.apache.commons.cli.*;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.lang.System.exit;

/**
 * Hello world!
 */
public class ImageScaler {

    static enum ScaleMode {
        GALLERY,
        THUMBNAIL
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageScaler.class);
    private static final String DEFAULT_PATH = System.getProperty("user.dir");

    private static final int[] GALLERY_WIDTHS = {1080, 750, 640};
    private static final int[] GALLERY_HEIGHTS = {810, 563, 480};

    private static final int[] THUMBNAIL_WIDTHS = {96, 128, 160};
    private static final int[] THUMBNAIL_HEIGHTS = {96, 128, 160};

    private static final int[] SIZE_TYPES = {3, 2, 1};

    public static void main(String[] args) {
        LOGGER.info("Image scaling app started");

        Options options = new Options();
        options.addOption("i", "input", true, "Input file or directory");
        options.addOption("m", "mode", true, "Rescale mode: 'gallery' of 'thumbnail'");
        options.addOption("h", "help", false, "Print help");
        CommandLineParser parser = new GnuParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOGGER.error("ParseException occurred", e);
            exit(1);
        }

        if (cmd.hasOption("h")) {
            printHelp(options);
            exit(0);
        }

        String filePath;
        if (cmd.hasOption("i")) {
            filePath = cmd.getOptionValue("i");
            LOGGER.debug("File path {} provided", filePath);
        } else {
            filePath = DEFAULT_PATH;
            LOGGER.debug("File path {} set to PWD", filePath);
        }

        ScaleMode scaleMode = null;
        if (cmd.hasOption("m")) {
            switch (cmd.getOptionValue("m")) {
                case "gallery":
                    scaleMode = ScaleMode.GALLERY;
                    break;
                case "thumbnail":
                    scaleMode = ScaleMode.THUMBNAIL;
                    break;
                default:
                    LOGGER.error("Please provide valid value for mode: 'gallery' or 'thumbnail'");
                    printHelp(options);
                    exit(0);
            }
            LOGGER.debug("Scaling mode set to {}", scaleMode);
        } else {
            LOGGER.error("Please provide -m or --mode option");
            printHelp(options);
            exit(0);
        }

        File inputFile = new File(filePath);
        if (inputFile.isFile()) {
            scaleCropSave(inputFile, scaleMode);
        } else if (inputFile.isDirectory()) {
            for (File file : inputFile.listFiles()) {
                if (file.isFile()) {
                    scaleCropSave(file, scaleMode);
                }
            }
        }

        LOGGER.info("Image scaling app ended");
    }


    private static void printHelp(Options options) {
        String header = "Scale & crop images to different sizes \n" +
                "  in mode gallery: 1080x810 750x563 640x480 \n" +
                "  in mode thumbnail: 96x96, 128x128, 160x160 \n\n";
        String footer = "\nPlease report issues to bartvanbos@allbits.org";
        String jarFileName = new java.io.File(ImageScaler.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath())
                .getName();
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(jarFileName, header, options, footer, true);
    }


    private static void scaleCropSave(File inputFile, ScaleMode scaleMode) {
        if (!isImageFile(inputFile)) return;

        String fileName = inputFile.getName();
        String nameNoExt = FilenameUtils.getBaseName(fileName);
        String extension = FilenameUtils.getExtension(fileName);
        LOGGER.debug("Opened file {}", inputFile.getAbsolutePath());

        BufferedImage sourceImage;
        try {
            sourceImage = ImageIO.read(inputFile);
            if (null == sourceImage) {
                LOGGER.error("Failed to properly read image {} with ImageIO");
                exit(1);
            }

            LOGGER.info("Going to scale and crop {}", fileName);

            for (int i = 0; i < GALLERY_WIDTHS.length; i++) {
                int width;
                int height;
                if (scaleMode.equals(ScaleMode.GALLERY)) {
                    width = GALLERY_WIDTHS[i];
                    height = GALLERY_HEIGHTS[i];
                } else {
                    width = THUMBNAIL_WIDTHS[i];
                    height = THUMBNAIL_HEIGHTS[i];
                }
                double targetAspectRatio = ((double) width / (double) height);

                BufferedImage bufferedScaled;
                BufferedImage bufferedCropped;
                if (((double) sourceImage.getWidth() / (double) sourceImage.getHeight()) > targetAspectRatio) {
                    bufferedScaled = scaleHeight(sourceImage, height);
                    bufferedCropped = cropWidth(bufferedScaled, width);
                } else {
                    bufferedScaled = scaleWidth(sourceImage, width);
                    bufferedCropped = cropHeight(bufferedScaled, height);
                }

                File outputFile = new File(nameNoExt + "-size-" + SIZE_TYPES[i] + "." + extension);
                try {
                    ImageIO.write(bufferedCropped, extension, outputFile);
                } catch (IOException e) {
                    LOGGER.error("IOException while saving scaled picture {}", outputFile.getAbsolutePath());
                }
            }
        } catch (IOException e) {
            LOGGER.error("IOException occurred", e);
            exit(1);
        }
    }


    private static BufferedImage scaleWidth(BufferedImage image, int targetWidth) {
        int actualWidth = image.getWidth();
        int actualHeight = image.getHeight();
        double imageRatio = (double) actualHeight / (double) actualWidth;
        LOGGER.debug("Going to scaleWidth image with width {} to width {}", actualWidth, targetWidth);

        ResampleOp resampleOp = new ResampleOp(targetWidth, (int) (targetWidth * imageRatio));
        resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal);
        BufferedImage bufferedScaled = resampleOp.filter(image, null);

        return bufferedScaled;
    }


    private static BufferedImage scaleHeight(BufferedImage image, int targetHeight) {
        int actualWidth = image.getWidth();
        int actualHeight = image.getHeight();
        double imageRatio = (double) actualWidth / (double) actualHeight;
        LOGGER.debug("Going to scaleHeight image with height {} to height {}", actualHeight, targetHeight);

        ResampleOp resampleOp = new ResampleOp((int) (targetHeight * imageRatio), targetHeight);
        resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal);
        BufferedImage bufferedScaled = resampleOp.filter(image, null);

        return bufferedScaled;
    }


    private static BufferedImage cropWidth(BufferedImage image, int targetWidth) {
        int actualWidth = image.getWidth();
        LOGGER.debug("Going to cropWidth image with width {} to width {}", actualWidth, targetWidth);

        int x = actualWidth / 2 - targetWidth / 2;
        int y = 0;
        int w = targetWidth;
        int h = image.getHeight();

        BufferedImage cropped = image.getSubimage(x, y, w, h);
        BufferedImage bufferedCropped = new BufferedImage(cropped.getWidth(), cropped.getHeight(), BufferedImage.TYPE_INT_RGB);
        bufferedCropped.getGraphics().drawImage(cropped, 0, 0, null);

        return bufferedCropped;
    }


    private static BufferedImage cropHeight(BufferedImage image, int targetHeight) {
        int actualHeight = image.getHeight();
        LOGGER.debug("Going to cropHeight image with height {} to height {}", actualHeight, targetHeight);

        int x = 0;
        int y = actualHeight / 2 - targetHeight / 2;
        int w = image.getWidth();
        int h = targetHeight;

        BufferedImage cropped = image.getSubimage(x, y, w, h);
        BufferedImage bufferedCropped = new BufferedImage(cropped.getWidth(), cropped.getHeight(), BufferedImage.TYPE_INT_RGB);
        bufferedCropped.getGraphics().drawImage(cropped, 0, 0, null);

        return bufferedCropped;
    }


    private static boolean isImageFile(File file) {
        try {
            return (ImageIO.read(file)) != null;
        } catch (Exception e) {
            LOGGER.error("Exception occurred reading file " + file.getName(), e);
        }

        return false;
    }


}
